import React, { useState } from "react";
import { useGlobalAuthContext } from "../../context/authContext";
import { initialState } from "./constants";
import "./login.css";
import { CredentialTypes } from "./loginTypes";

const Login = () => {
  const [credentials, setCredentials] = useState<CredentialTypes>(initialState);

  const {authState, dispatch} = useGlobalAuthContext();
  const {error, loading} = authState; 

  const handleChange = ()=>{

  };

  return (
    <div className="login">
        <div className="lContainer">
            <input type="text" className="lInput" placeholder="username" id="username" onChange={handleChange} />
            <input type="password" className="lInput" placeholder="password" id="password" onChange={handleChange} />
            <button className="lButton">Login</button>
            {error && <span>{`${error}`}</span>}
        </div> 
    </div>
  );
};

export default Login;
