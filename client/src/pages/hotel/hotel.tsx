import {
  faCircleArrowLeft,
  faCircleArrowRight,
  faCircleXmark,
  faLocationDot,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import Header from "../../components/header/header";
import Navbar from "../../components/navbar/navbar";
// import { photos } from "./photos";
import "./hotel.css";
import useFetch from "../../hooks/useFetch";
import { useLocation } from "react-router-dom";
import { useGlobalContext } from "../../context/searchContext";
import { dayDifference } from "../../utilities/common";

type DirectionType = "l" | "r";

const Hotel = () => {
  const location = useLocation();
  const {dates, options} = useGlobalContext();
  const id = location.pathname.split("/")[2];
  
  const [open, setOpen] = useState<boolean>(false);
  const [slideNumber, setSlideNumber] = useState<number>(0);

  const [data, loading, error] = useFetch(`/hotels/find/${id}`);

  let days;
  if(dates && dates.length){
    days = dayDifference(dates[0].endDate as Date, dates[0].startDate as Date);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleMove = (direction: DirectionType) => {
    let newSlideNumber;
    if (direction === "l") {
      newSlideNumber = slideNumber === 0 ? 5 : slideNumber - 1;
    } else {
      newSlideNumber = slideNumber === 5 ? 0 : slideNumber + 1;
    }
    setSlideNumber(newSlideNumber);
  };

  const handleOpen = (i: number) => {
    setSlideNumber(i);
    setOpen(true);
  };

  console.log(error);

  return (
    <div>
      <Navbar />
      <Header type="list" />
      {loading ? (
        "loading please wait"
      ) : (
        <div className="hotelContainer">
          {open && (
            <div className="slider">
              <FontAwesomeIcon
                icon={faCircleXmark}
                className="close"
                onClick={handleClose}
              />
              <FontAwesomeIcon
                icon={faCircleArrowLeft}
                className="arrow"
                onClick={() => handleMove("l")}
              />
              <div className="sliderWrapper">
                <img
                  src={data[0]?.photos[slideNumber]}
                  alt=""
                  className="sliderImg"
                />
              </div>
              <FontAwesomeIcon
                icon={faCircleArrowRight}
                className="arrow"
                onClick={() => handleMove("r")}
              />
            </div>
          )}
          <div className="hotelWrapper">
            <button className="bookNow">Reserve or Book Now!</button>
            <h1 className="hotelTitle">{data[0]?.name}</h1>
            <div className="hotelAddress">
              <FontAwesomeIcon icon={faLocationDot} />
              <span>{data[0]?.address}</span>
            </div>
            <span className="hotelDistance">
              Excellent location – {data[0]?.distance}m from center
            </span>
            <span className="hotelPriceHighlight">
              Book a stay over ${data[0]?.cheapestPrice} at this property and get a free airport taxi
            </span>
            <div className="hotelImages">
              {data[0]?.photos?.map((photo: string, i: number) => (
                <div className="hotelImgWrapper" key={i}>
                  <img
                    onClick={() => handleOpen(i)}
                    src={photo}
                    alt=""
                    className="hotelImg"
                  />
                </div>
              ))}
            </div>
            <div className="hotelDetails">
              <div className="hotelDetailsTexts">
                <h1 className="hotelTitle">{data[0]?.title}</h1>
                <p className="hotelDesc">
                 {data[0]?.desc}
                </p>
              </div>
              <div className="hotelDetailsPrice">
                <h1>Perfect for a {days}-night stay!</h1>
                <span>
                  Located in the real heart of Krakow, this property has an
                  excellent location score of {data[0]?.rating}!
                </span>
                {options && days&&
                <h2>
                  <b>${days * data[0]?.cheapestPrice * options?.room}</b> ({days} nights)
                </h2>}
                <button>Reserve or Book Now!</button>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Hotel;
