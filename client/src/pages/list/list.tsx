import { format } from "date-fns";
import { useState } from "react";
import { useLocation } from "react-router-dom";
import Header from "../../components/header/header";
import Navbar from "../../components/navbar/navbar";
import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css"; // theme css file
import { DateRange } from "react-date-range";
import "./list.css";
// import SearchItem from "../../components/searchItem/searchItem";
// import useFetch from "../../hooks/useFetch";

type DateSelectionType = {
  selection: {
    startDate: Date;
    endDate: Date;
    key: "selection";
  };
};

const List = () => {
  const location = useLocation();
  const destination: string = location.state.destination
  // const [destination, setDestination] = useState<string>(
  //   location.state.destination
  // );
  const [openDate, setOpenDate] = useState<boolean>(false);
  const [date, setDate] = useState(location.state.date);
  // const [options, setOptions] = useState(location.state.options);
  const options = location.state.options;

  const handleOpen = () => {
    setOpenDate((prev) => !prev);
  };

  // const handleMin = (e: React.ChangeEvent<HTMLInputElement>)=>{
  //   setMin(Number(e.target.value));
  // }

  // const handleMax = (e: React.ChangeEvent<HTMLInputElement>)=>{
  //   setMax(Number(e.target.value));
  // }

  // const handleSearch = ()=>{
  //   reFetch();
  // }

  return (
    <div>
      <Navbar />
      <Header type="list" />
      <div className="listContainer">
        <div className="listWrapper">
          <div className="listSearch">
            <h1 className="lsTitle">Search</h1>
            <div className="lsItem">
              <label>Destination</label>
              <input type="text" placeholder={destination} />
            </div>
            <div className="lsItem">
              <label>Check-in Date</label>
              <span onClick={() => handleOpen}>
                {`${format(date[0].startDate, "MM/dd/yyyy")} to ${format(
                  date[0].endDate,
                  "MM/dd/yyyy"
                )}`}
              </span>
              {openDate && (
                <DateRange
                  onChange={(item: DateSelectionType) =>
                    setDate([item.selection])
                  }
                  minDate={new Date()}
                  ranges={date}
                />
              )}
            </div>
            <div className="lsItem">
              <label>Options</label>
              <div className="lsOptions">
                <div className="lsOptionItem">
                  <span className="lsOptionText">
                    Min price <small>per night</small>
                  </span>
                  <input type="number" className="lsOptionInput" />
                </div>
                <div className="lsOptionItem">
                  <span className="lsOptionText">
                    Max price <small>per night</small>
                  </span>
                  <input type="number" className="lsOptionInput" />
                </div>
                <div className="lsOptionItem">
                  <span className="lsOptionText">Adult</span>
                  <input
                    type="number"
                    min={1}
                    className="lsOptionInput"
                    placeholder={options.adult}
                  />
                </div>
                <div className="lsOptionItem">
                  <span className="lsOptionText">Children</span>
                  <input
                    type="number"
                    min={0}
                    className="lsOptionInput"
                    placeholder={options.children}
                  />
                </div>
                <div className="lsOptionItem">
                  <span className="lsOptionText">Room</span>
                  <input
                    type="number"
                    min={1}
                    className="lsOptionInput"
                    placeholder={options.room}
                  />
                </div>
              </div>
            </div>
            <button>Search</button>
          </div>
          {/* <div className="listResult">
            {loading ? "Loading please wait" : 
            data.length ? 
            data.map((eachItem, i)=>{
              return (
                <SearchItem key={i} item={eachItem}/>
                )
            }) : "No Hotels Available"
              }
          </div> */}
        </div>
      </div>
    </div>
  );
};

export default List;
