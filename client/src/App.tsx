import { BrowserRouter, Route, Routes } from "react-router-dom";
import { AuthContextProvider } from "./context/authContext";
import { SearchContextProvider } from "./context/searchContext";
import Home from "./pages/home/home";
import Hotel from "./pages/hotel/hotel";
import List from "./pages/list/list";

function App() {
  return (
    <AuthContextProvider>
      <SearchContextProvider>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/hotels" element={<List />} />
            <Route path="/hotels/:id" element={<Hotel />} />
          </Routes>
        </BrowserRouter>
      </SearchContextProvider>
    </AuthContextProvider>
  );
}

export default App;
