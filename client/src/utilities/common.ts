export const MillisecondsPerDay: number = 1000 * 60 * 60 * 24;

export const dayDifference = (date1:Date, date2:Date)=>{
    const timeDiff = Math.abs(date2.getTime()-date1.getTime());
    const dayDiff = Math.ceil(timeDiff / MillisecondsPerDay);
    return dayDiff;
};