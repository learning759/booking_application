import axios from "axios";
import { useEffect, useState } from "react";

const useFetch = (url: string="") => {
  const [data, setData] = useState<any[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<boolean>(false);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const res = await axios.get(url);
        if (res.data.length === undefined) {
          setData([res.data]);
        }else{
          setData(res.data);
        }
      } catch (err) {
        console.log(err);
        setError(true);
      }
      setLoading(false);
    };
    fetchData();
  }, [url]);

  const reFetch = async () => {
    setLoading(true);
    try {
      const res = await axios.get(url);
      setData(res.data);
    } catch (err) {
      console.log(err);
      setError(true);
    }
    setLoading(false);
  };

  return [data, loading, error, reFetch] as const;
};

export default useFetch;
