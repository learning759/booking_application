
export const Action = {
    NEW_SEARCH: "new_search",
    RESET_SEARCH: "reset_search",
    LOGIN_START: "login_Start",
    LOGIN_SUCESS: "login_success",
    LOGIN_FAILED: "login_failed",
    LOGIN_LOGOUT: "logged_out"
};

export const INITIAL_STATE = {
    city: "",
    dates: [],
    options: {
        adult: 0,
        children: 0,
        room: 0,
    },
};

export const AUTH_INITIAL_STATE = {
    user: JSON.parse(localStorage.getItem('user') || "{}") ,
    loading: false,
    error: {}
}