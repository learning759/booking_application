export type DateType = {
    startDate: Date,
    endDate: Date,
    key: "selection"
}[];

export type ReducerStateType = {
    city: string;
    dates: DateType;
    options: {
        adult: number;
        children: number;
        room: number;
    };
};

export type AuthReducerStateType = {
    user: null | {};
    loading: boolean;
    error: {}
};

export type ReducerActionType = {
    type: string,
    payload: ReducerStateType
};

export type AuthReducerActionType = {
    type: string,
    payload: AuthReducerStateType
};

export type SearchContextProviderProps = {
    children: React.ReactNode;
};
export type AuthContextProviderProps = {
    children: React.ReactNode;
};

export type ContextType = {
    city: string,
    dates: DateType,
    options: {
        adult: number;
        children: number;
        room: number;
    },
    dispatch: React.Dispatch<ReducerActionType>
};

export type AuthContextType = {
    authState: AuthReducerStateType,
    dispatch: React.Dispatch<AuthReducerActionType>
};

