import { Action, INITIAL_STATE, AUTH_INITIAL_STATE } from "./constants";
import { AuthReducerActionType, AuthReducerStateType, ReducerActionType, ReducerStateType } from "./contextTypes";

export const searchReducer = (
    state: ReducerStateType,
    action: ReducerActionType
) => {
    switch (action.type) {
        case Action.NEW_SEARCH:
            return action.payload;
        case Action.RESET_SEARCH:
            return INITIAL_STATE;
        default:
            return state;
    }
};


export const authReducer = (
    state: AuthReducerStateType,
    action: AuthReducerActionType
) => {
    switch (action.type) {
        case Action.LOGIN_START:
            return { user: {}, error: {}, loading: true };
        case Action.LOGIN_SUCESS:
            return { user: action.payload, error: {}, loading: false };
        case Action.LOGIN_FAILED:
            return { loading: false, error: action.payload, user: {} };
        case Action.LOGIN_LOGOUT:
            return AUTH_INITIAL_STATE;
        default:
            return state;
    }
};