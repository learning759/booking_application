import React, { createContext, useContext, useEffect, useReducer } from 'react'
import { AuthContextProviderProps, AuthContextType } from './contextTypes'
import { AUTH_INITIAL_STATE } from './constants'
import { authReducer } from './reducer';

export const AuthContext = createContext<AuthContextType>({authState: AUTH_INITIAL_STATE, dispatch: ()=>void 0});

export const AuthContextProvider = ({children}: AuthContextProviderProps) => {
  const [state, dispatch] = useReducer(authReducer, AUTH_INITIAL_STATE);

  useEffect(()=>{
    localStorage.setItem("user", JSON.stringify(state.user));
  }, [state.user]);

  return (
    <AuthContext.Provider value={{authState: state, dispatch}}>
      {children}
    </AuthContext.Provider>
  );
};

export const useGlobalAuthContext = ()=>{
  const authContext = useContext(AuthContext);
  if(authContext){
    return authContext;
  };

  return {authState: AUTH_INITIAL_STATE,dispatch: ()=>void 0 };
};

export default AuthContext