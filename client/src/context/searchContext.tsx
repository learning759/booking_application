import React, { createContext, useContext, useReducer } from "react";
import { INITIAL_STATE } from "./constants";
import { ContextType, SearchContextProviderProps } from "./contextTypes";
import { searchReducer } from "./reducer";

// create context
export const SearchContext = createContext<ContextType>({...INITIAL_STATE, dispatch: ()=>void 0});

// creating context provider
export const SearchContextProvider = ({
  children,
}: SearchContextProviderProps) => {
  const [state, dispatch] = useReducer(searchReducer, INITIAL_STATE);
  return (
    <SearchContext.Provider
      value={{
        city: state.city,
        dates: state.dates,
        options: state.options,
        dispatch,
      }}
    >
      {children}
    </SearchContext.Provider>
  );
};

// simplifying context calling
export const useGlobalContext = () => {
  const appContext = useContext(SearchContext);
  if (appContext) {
    return appContext;
  }

  return {...INITIAL_STATE, dispatch: ()=>void 0};
};
