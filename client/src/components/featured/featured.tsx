import useFetch from "../../hooks/useFetch";
import { featImages } from "./featImages";
import "./featured.css";

const Featured = () => {
  const [data, loading] = useFetch(
    "/hotels/countByCity?cities=trivandrum,kollam,kochi"
  );

  const cities: string[] = ["Trivandrum", "Kollam", "Kochi"];

  return (
    <div className="featured">
      {loading ? (
        "Loading please wait"
      ) : (
        <>
          {data.map((eachItem, i) => {
            return (
              <div className="featuredItem" key={i}>
                <img src={featImages[i]} alt="" className="featuredImg" />
                <div className="featuredTitles">
                  <h1>{cities[i]}</h1>
                  <h2>{eachItem} properties</h2>
                </div>
              </div>
            );
          })}
          ;
        </>
      )}
    </div>
  );
};

export default Featured;
