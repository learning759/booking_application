
export type HeaderProps = {
    type: "list" | "";
};

export type DateType = {
    startDate: Date;
    endDate: Date;
    key: "selection";
}[];

export type OptionType = {
    adult: number;
    children: number;
    room: number;
};

export type DateSelectionType = {
    selection: {
        startDate: Date;
        endDate: Date;
        key: "selection";
    };
};

export type OpenOptionNameType = "adult" | "children" | "room";

export type OpenOptionOperationType = "d" | "i";