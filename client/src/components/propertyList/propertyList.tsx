import useFetch from "../../hooks/useFetch";
import { images } from "./images";
import "./propertyList.css";

const PropertyList = () => {
  const [data, loading] = useFetch("/hotels/countByType");

  return (
    <div className="pList">
      {loading ? (
        "loading please wait"
      ) : (
        <>
          {data.map((eachItem, i) => {
            return (
              <div className="pListItem" key={i}>
                <img src={images[i]} alt="" className="pListImg" />
                <div className="pListTitles">
                  <h1>{eachItem?.type}</h1>
                  <h2>{eachItem?.count} {eachItem?.type}</h2>
                </div>
              </div>
            );
          })}
        </>
      )}
    </div>
  );
};

export default PropertyList;
