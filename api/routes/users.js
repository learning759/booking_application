import express from 'express';
import { deleteUser, getUser, listUsers, updateUser } from '../controllers/user.js';
import { verifyAdmin, verifyToken, verifyUser } from '../utils/verifyToken.js';


// create a router from express
const router = express.Router();

// end points for verify user and admin
router.get("/checkauthentication", verifyToken, (req, res, next)=>{
    res.send("Hello user, you are logged in")
});

router.get("/checkuser/:id", verifyUser, (req, res, next)=>{
    res.send("Hello user, You are logged in and you can delete your account")
});

router.get("/checkadmin/:id", verifyAdmin, (req, res, next)=>{
    res.send("Hello user, You are logged in and you can delete all accounts")
});


// UPDATE
router.put("/:id",verifyUser, updateUser);

// DELETE
router.delete("/:id", verifyUser, deleteUser);

// GET
router.get("/:id", verifyUser, getUser);

// GET ALL
router.get("/", verifyAdmin, listUsers);


export default router;


