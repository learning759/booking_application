import express from 'express';
import { createRoom, deleteRoom, getRoom, listRoom, updateRoom } from '../controllers/room.js';
import { verifyAdmin } from '../utils/verifyToken.js';


// create a router from express
const router = express.Router();

router.post("/:hotelid", verifyAdmin, createRoom);
router.put("/:id", verifyAdmin, updateRoom);
router.delete("/:id/:hotelid", verifyAdmin, deleteRoom);
router.get("/:id",  getRoom);
router.get("/", listRoom);


export default router;


