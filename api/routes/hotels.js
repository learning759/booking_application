import express from 'express';
import { coutByCity, coutByType, createHotel, deleteHotel, getHotel, listHotel, updateHotel } from '../controllers/hotel.js';
import { verifyAdmin } from '../utils/verifyToken.js';


// create a router from express
const router = express.Router();

// CREATE
router.post("/", verifyAdmin, createHotel);

// UPDATE
router.put("/:id",verifyAdmin, updateHotel);

// DELETE
router.delete("/:id",verifyAdmin, deleteHotel);

// GET
router.get("/find/:id", getHotel);

// GET ALL
router.get("/", listHotel);


router.get("/countByCity", coutByCity);
router.get("/countByType", coutByType);

export default router;

