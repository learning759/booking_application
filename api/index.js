import express from 'express';
import dotenv from 'dotenv';
import mongoose from 'mongoose';
import authRoutes from './routes/auth.js';
import usersRoutes from './routes/users.js';
import hotelsRoutes from './routes/hotels.js';
import roomsRoutes from './routes/rooms.js';
import cookieParser from 'cookie-parser';
import cors from 'cors';


// creating an express instance  
const app = express();

// configuring .env file
dotenv.config();


// connecting mongo db
const connect = () => {
    try{
        mongoose.connect(process.env.MONGO);
        console.log("connected to mongodb!")
    }catch(err){
        throw err;
    }
}

// show msg when mongodb disconnected
mongoose.connection.on("disconnected", ()=>{
    console.log("Oops..! mongodb disconnected");
})

// show msg when mongodb connected
mongoose.connection.on("connected", ()=>{
    console.log("mongodb connected");
})

// Middlewares
app.use(cors());
app.use(cookieParser());
app.use(express.json());
app.use("/api/auth", authRoutes);
app.use("/api/users", usersRoutes);
app.use("/api/hotels", hotelsRoutes);
app.use("/api/rooms", roomsRoutes);

// error handling middleware
app.use((err, req, res, next)=>{
    const errorStatus = err.status || 500;
    const errorMessage = err.message || "Something went wrong!";
    return res.status(errorStatus).json({
        success: false,
        status: errorStatus,
        message: errorMessage,
        stack: err.stack
    });
})

// listening app to a particular port
app.listen(8800, ()=>{
    connect();
    console.log("Connected to backend!");
})
