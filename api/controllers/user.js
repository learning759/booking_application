import User from '../models/User.js';

export const updateUser = async (req, res, next)=>{
    try {
        const updateUser = await User.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true});
        res.status(200).json(updateUser);
    } catch (err) {
        next(err);
    };
};

export const deleteUser = async (req, res, next)=>{
    try {
      const deleteUser = await User.findByIdAndDelete(req.params.id);  
      res.status(200).json("User has been deleted!");
    } catch (err) {
        next(err);
    };
};

export const getUser = async (req, res, next)=>{
    try {
        const getUser = await User.findById(req.params.id);
        const {password, ...otherProperties} = getUser._doc;
        res.status(200).json({...otherProperties});
    } catch (err) {
      next(err);  
    };
};

export const listUsers = async (req, res, next)=>{
    try {
        const listUsers = await User.find();
        res.status(200).json(listUsers);
    } catch (err) {
      next(err);  
    };
};